# scan_matcher_ros
This package implements a 2D cloud matcher based on ICP. It uses the normals of the points as extended features to perform the optimization.

## Prerequisites:

This package requires:
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)
* [srrg_boss](https://gitlab.com/srrg-software/srrg_boss)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_core_ros](https://gitlab.com/srrg-software/srrg_core_ros)
* [srrg_core_viewers](https://gitlab.com/srrg-software/srrg_core_viewers)
* [srrg_gl_helpers](https://gitlab.com/srrg-software/srrg_gl_helpers)

## Applications:

* `srrg_scan_matcher_node`:

You can run the ROS node by simply doing
```sh
$ rosrun srrg_scan_matcher_ros srrg_scan_matcher_node
```
You might need to setup parameters like `laser_topic`, `odom_topic`, ... 
Alternatively, you can use the following launch file by

```sh
$ roslaunch srrg_scan_matcher_ros srrg_scan_matcher.launch
```

tf transform from `laser` to `base_link` is required.
